---
- name: "apt: install metrics dependencies"
  apt:
    state: latest
    autoclean: yes
    autoremove: yes
    install_recommends: no
    name:
      - python3-dateutil
      - python3-git
      - python3-jinja2
      - python3-yaml
      - sudo
      - xz-utils

- name: "apt: install metrics dependencies"
  apt:
    state: latest
    autoclean: yes
    autoremove: yes
    install_recommends: no
    name:
      - python3-ua-parser
      - python3-user-agents

- name: "known_hosts: gitlab.com rsa"
  known_hosts:
    path: /etc/ssh/ssh_known_hosts
    name: gitlab.com
    key: altssh.gitlab.com,gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9

- name: "known_hosts: gitlab.com ecdsa"
  known_hosts:
    path: /etc/ssh/ssh_known_hosts
    name: gitlab.com
    key: altssh.gitlab.com,gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=

- name: "known_hosts: gitlab.com ed25519"
  known_hosts:
    path: /etc/ssh/ssh_known_hosts
    name: gitlab.com
    key: altssh.gitlab.com,gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf

- name: "user: add metrics"
  user:
    name: metrics
    group: adm
    comment: F-Droid Metrics Bot
    shell: /bin/bash

- name: "file: make ~metrics/.ssh"
  file:
    path: "{{ metricshome }}/.ssh"
    state: directory
    mode: 0755
    owner: root
    group: root

- name: "copy: ssh config"
  copy:
    mode: 0640
    owner: root
    group: adm
    content: |
      # hide in port 443 https://docs.gitlab.com/ee/user/gitlab_com/
      # https://about.gitlab.com/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/
      Host gitlab.com
          Hostname altssh.gitlab.com
          Compression yes
          User git
          Port 443
          ServerAliveInterval 30
          ServerAliveCountMax 60
          IdentitiesOnly yes
          IdentityFile /home/metrics/.ssh/id_ed25519
    dest: "{{ metricshome }}/.ssh/config"

- name: "git: clone metrics"
  git:
    repo: 'https://gitlab.com/fdroid/metrics.git'
    dest: "{{ metricshome }}/metrics"
    version: master

- name: "git: clone apache-log-parser"
  git:
    repo: 'https://gitlab.com/cleaninsights/apache-log-parser.git'
    dest: "{{ metricshome }}/apache-log-parser"
    version: master

- name: "file: restrict write privs for metrics user"
  file:
    dest: "{{ metricshome }}"
    owner: root
    group: root
    mode: u+rwX,g+rX,o+rX,go-w
    recurse: yes

- name: "file: set up writeable data dir for metrics user"
  file:
    dest: "{{ metricshome }}/data"
    state: directory
    owner: metrics
    group: adm
    mode: u+rwX,g+rX,o+rX,o-w
    recurse: yes

- name: "file: set up writeable known_hosts for metrics user"
  file:
    dest: "{{ metricshome }}/.ssh/known_hosts"
    state: touch
    owner: metrics
    group: adm
    mode: 0600

- name: "check if nginx is webserver"
  stat:
    path: /var/log/nginx/
  ignore_errors: True
  register: nginx

- name: "point to nginx logs"
  set_fact:
    logdir: /var/log/nginx/
  when: nginx.stat.exists

- name: "check if apache2 is webserver"
  stat:
    path: /var/log/apache2/
  register: apache2
  when: not nginx.stat.exists

- name: "point to apache2 logs"
  set_fact:
    logdir: /var/log/apache2/
  when: not nginx.stat.exists and apache2.stat.exists

- name: "copy: metrics cron job"
  copy:
    content: |
      # The metric reporting period is Monday 0:00 til Sunday 23:59:59.
      # This is run delayed to give us extra time in case we need to remove
      # privacy leaks before things are published.  Also, this is run on
      # Tuesday since it only processes *.log.gz and yesterday's log is not
      # compressed in Debian default configurations.  This is run at 12:30
      # since by default logrotate is run via /etc/cron.daily, and that is
      # run by default at around 6:30.
      LANG=C.UTF-8
      PYTHONPATH={{ metricshome }}/apache-log-parser
      30 12 * * Tue metrics  cd {{ metricshome }}/data && {{ metricshome }}/metrics/compile-logs-into-hits.py {{ logdir }} {{ inventory_hostname }}  > /tmp/fdroid-metrics.log 2>&1
    mode: 0644
    owner: root
    group: root
    dest: /etc/cron.d/fdroid-metrics

- name: "file: set permissions on /tmp/fdroid-metrics.log"
  file:
    path: /tmp/fdroid-metrics.log
    state: touch
    mode: 0644
    owner: metrics
    group: adm

- name: "file: remove old cron.weekly job"
  file:
    state: absent
    dest: /etc/cron.weekly/fdroid-metrics

- name: "command: generate ED25519 deploy key"
  command: "ssh-keygen -q -t ed25519 -f {{ metricshome }}/.ssh/id_ed25519 -C {{ inventory_hostname }} -N ''"
  args:
    creates: "{{ metricshome }}/.ssh/id_ed25519"

- name: "file: allow metrics user to read its ssh keys"
  file:
    dest: "{{ metricshome }}/.ssh"
    owner: root
    group: adm
    mode: u+rwX,g+rX,o-rx,go-w
    recurse: yes

- name: Slurp SSH deploy public key file
  slurp:
    src: "{{ metricshome }}/.ssh/id_ed25519.pub"
  register: deploykey

- name: "Add this key to the gitlab project with write access!"
  debug:
    msg:
      - "https://gitlab.com/fdroid/metrics-data/{{ inventory_hostname }}/-/settings/repository#js-deploy-keys-settings"
      - "{{ deploykey['content'] | b64decode }}"
