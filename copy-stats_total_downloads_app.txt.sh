#!/bin/sh
#
# Run this in a local clone of https://gitlab.com/fdroid/fdroiddata.git
# to get a copy of each time stats/total_downloads_app.txt was changed.

set -e
set -x

git checkout master
for commitid in `git log --reverse --format='%H' -- stats/total_downloads_app.txt`; do
    timestamp=`git log -n1 --format='%ct' $commitid`
    git checkout $commitid
    cp stats/total_downloads_app.txt \
       $HOME/code/fdroid/metrics/per-commit/${timestamp}-total_downloads_app.txt
done
