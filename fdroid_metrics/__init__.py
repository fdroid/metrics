
import dateutil.parser
import git
import glob
import jinja2
import json
import os
from datetime import datetime, timezone


REMOTE_URL_FORMAT = 'git@gitlab.com:fdroid/metrics-data/{domain}.git'
DATA_JSON_GLOB = '[1-9][0-9][0-9][0-9]-[0-3][0-9]-[0-3][0-9].json'


def get_metrics_data_repo(domain):
    remote_url = REMOTE_URL_FORMAT.format(domain=domain)
    if os.path.exists(domain) and not os.path.isdir(domain):
        raise Exception(
            'A file called {domain} is blocking the creation of the metrics-data dir!'
        )
    if not os.path.exists(domain):
        git.Repo.clone_from(remote_url, domain)
    return git.Repo.init(domain)


def pull_commits(git_repo):
    git.remote.Remote(git_repo, 'origin').pull()
    return git_repo


def commit_compiled_json(git_repo):
    basedir = os.path.dirname(git_repo.git_dir)
    for f in git_repo.git().ls_files(modified=True).split():
        if os.path.getsize(os.path.join(basedir, f)) == 0:
            print('Skipping 0 size file:', f)
            continue
        git_repo.index.add([f])
        git_repo.index.commit('update ' + f)
    for f in git_repo.untracked_files:
        if os.path.getsize(os.path.join(basedir, f)) == 0:
            print('Skipping 0 size file:', f)
            continue
        git_repo.index.add([f])
        git_repo.index.commit('add ' + f)


def push_commits(git_repo):
    origin = git.remote.Remote(git_repo, 'origin')
    origin.push()


def get_hit_bucket_file_name(domain, start_date):
    if not os.path.exists(domain):
        os.mkdir(domain)
    return os.path.join(domain, start_date + '.json')


def get_week_start_date(datetimeobj):
    """normalize date to the Monday of the week"""
    dt = datetime.strptime(datetimeobj.strftime('%Y %W 1'), '%Y %W %w')
    return dt.replace(tzinfo=timezone.utc)


def render_index_json(domain):
    index = []
    for f in glob.glob(get_hit_bucket_file_name(domain, '*')):
        index.append(os.path.basename(f))
    with open(get_hit_bucket_file_name(domain, 'index'), 'w') as fp:
        json.dump(sorted(index, reverse=True), fp, indent=2)


def render_index_html(domains):
    template = os.path.join(
        os.path.abspath(os.path.dirname(__file__)), 'templates', 'index.html'
    )
    with open(template) as fp:
        jinja2.Template(fp.read()).stream(
            CI_PROJECT_NAMESPACE=os.getenv('CI_PROJECT_NAMESPACE'), domains=domains
        ).dump('index.html')


def load_hit_bucket_json(f):
    with open(f) as fp:
        return json.load(fp, object_hook=datetime_hook)


def datetime_hook(d):
    for k, v in d.items():
        try:
            d[k] = dateutil.parser.parse(v)
        except (AttributeError, TypeError, ValueError):
            pass
    return d


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        elif isinstance(obj, datetime):
            return obj.replace(tzinfo=timezone.utc).isoformat()
        return super().default(obj)
