
This is where F-Droid metrics data is gathered and deployed to GitLab
Pages.  The general policy is that no Personally Identifiable
Information (PII) should be stored here.  All of the data is unified
into a single file per site and published to
<https://fdroid.gitlab.io/metrics>.  The individual, per-week files
are kept in git here.

The basic setup is first, set up the webserver with a
privacy-preserving log format,
([nginx](https://f-droid.org/2019/04/15/privacy-preserving-analytics.html),
or
[apache](https://guardianproject.info/2017/06/08/tracking-usage-without-tracking-people)).
Then those logs are processed using a [sanitizing
script](compile-logs-into-hits.py) inspired by Tor's
[sanitize.py](https://gitweb.torproject.org/webstats.git/tree/src/sanitize.py).


## Data Storage

This uses git repos for each site to store the resulting JSON files.
These git repos are setup to only contain those files, and allow
_fdroid-bot_ to push commits to them.  No code is contained or
executed from them, they are append-only logs for the data.  They are
all contained in the
[fdroid-metrics-data](https://gitlab.com/fdroid-metrics-data/) group.


## Code

The tests are standard Python tests, and can be run using `python3 -m
unittest`.  The code format is `black --skip-string-normalization`.


## Deploying to servers

This repo also includes an _ansible_
"[role](https://galaxy.ansible.com/docs/contributing/creating_role.html)"
that can be added to a "playbook" for a server.  This assumes the
server is running Debian (_bookworm_, _bullseye_, or _buster_).  It also
assumes the webserver used is either _apache2_ or _nginx_.  Before
setting up the server, there needs to be a git repo with a README in
it in https://gitlab.com/fdroid/metrics-data that is named after the
fully qualified domain name of the server,
(e.g. _verification.f-droid.org_).  It will generate the SSH Deploy
Key, and print out the gitlab.com URL and public to add there.


## Uploading to metrics.cleaninsights.org CIMP

```console
$ git clone https://gitlab.com/fdroid/metrics.git
$ cd metrics
$ git clone git@gitlab.com:fdroid-metrics-data/mirror.f-droid.org.git
$ cd mirror.f-droid.org
$ ../submit-to-cimp.py post
```

`../submit-to-cimp.py` can be safely run for testing when using without the
`post` argument since it will only operate locally.  The `post` argument is
required to actually send data to the CIMP.  Also, this script will only send
data from completed reporting periods (e.g. a full week from Monday 0:00:00 til
Sunday 23:59:59).


## _stats/total_downloads_app.txt_

From October 2011 til May 2015, some download counts were kept in _fdroiddata_ in the file _stats/total_downloads_app.txt_.  There are two scripts for extracting info from those commits:

* `./copy-stats_total_downloads_app.txt.sh`
* `./parse-stats_total_downloads_apps.py`
